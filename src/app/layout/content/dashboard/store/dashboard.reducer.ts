import { AppState } from 'src/app/store/app.reducer';
import { Device } from '../../device/model/device';
import { DashboardAction, DashboardActionType } from './dashboard.action';

export interface AppDashboardState extends AppState {
  DashboardState: DashboardState;
}
export interface DashboardState {
  selectSensorType: string;
  selectDevices: Device[];
  loading: boolean;
  devices: Device[];
  error?: any;
}

export const initialDashboardState: DashboardState = {
  selectSensorType: 'temperature',
  loading: false,
  devices: [],
  selectDevices: []
};

export function DashboardReducer(
  state = initialDashboardState,
  action: DashboardAction
): DashboardState {
  switch (action.type) {
    case DashboardActionType.selectSensorType: {
      return { ...state, selectSensorType: action.payload.sensorType };
    }
    case DashboardActionType.selectDateTime: {
      return state;
    }
    case DashboardActionType.getDevice: {
      return state;
    }
    case DashboardActionType.getDeviceSuccess: {
      return { ...state, devices: action.payload };
    }
    case DashboardActionType.getDeviceError: {
      return state;
    }
    case DashboardActionType.selectDevices: {
      return { ...state, selectDevices: action.payload.devices };
    }
    default:
      return state;
  }
}
