import { Action } from '@ngrx/store';
import { Device } from '../../device/model/device';

export enum DashboardActionType {
  selectSensorType = '[Dashboard] select Sensor type',
  selectDateTime = '[Dashboard] select DateTime',
  selectDevices = '[Dashboard] select devices',
  getDevice = '[Dashboard] get device',
  getDeviceSuccess = '[Dashboard] get device success',
  getDeviceError = '[Dashboard] get device error'
}
export class SelectSensorType implements Action {
  readonly type = DashboardActionType.selectSensorType;
  constructor(public payload: { sensorType: string }) {}
}
export class SelectDateTime implements Action {
  readonly type = DashboardActionType.selectDateTime;
  constructor(public payload: { startDate: string; endDate: string }) {}
}
export class SelectDevices implements Action {
  readonly type = DashboardActionType.selectDevices;
  constructor(public payload: { devices: Device[] }) {}
}
export class GetDevices implements Action {
  readonly type = DashboardActionType.getDevice;
}
export class GetDevicesSuccess implements Action {
  readonly type = DashboardActionType.getDeviceSuccess;
  constructor(public payload: Device[]) {}
}
export class GetDevicesError implements Action {
  readonly type = DashboardActionType.getDeviceError;
  constructor(public payload: { error: any }) {}
}

export type DashboardAction =
  | SelectSensorType
  | SelectDateTime
  | SelectDevices
  | GetDevices
  | GetDevicesSuccess
  | GetDevicesError;
