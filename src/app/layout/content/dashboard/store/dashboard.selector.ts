import { createFeatureSelector, createSelector } from '@ngrx/store';
import { DashboardState } from './dashboard.reducer';

export const getDashboardState = createFeatureSelector<DashboardState>(
  'dashboard'
);
export const getDashboardType = createSelector(
  getDashboardState,
  (state: DashboardState) => state.selectSensorType
);
export const getLoading = createSelector(
  getDashboardState,
  (state: DashboardState) => state.loading
);
export const getDevices = createSelector(
  getDashboardState,
  (state: DashboardState) => state.devices
);
export const getSelectDevices = createSelector(
  getDashboardState,
  (state: DashboardState) => state.selectDevices
);
