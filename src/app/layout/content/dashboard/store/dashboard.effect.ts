import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  switchMap,
  withLatestFrom,
  map,
  catchError,
  mergeMap,
  tap,
  concatMap
} from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { DeviceService } from '../../device/service/device.service';
import { of, concat, merge } from 'rxjs';
import { LoadingShow, LoadingHide } from 'src/app/store/app.action';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {
  DashboardAction,
  DashboardActionType,
  GetDevicesSuccess,
  GetDevicesError
} from './dashboard.action';
import { DashboardState } from './dashboard.reducer';
@Injectable()
export class DashboardEffect {
  constructor(
    private actions$: Actions<DashboardAction>,
    private store: Store<DashboardState>,
    private deviceService: DeviceService,
    private alert: ToastrService,
    private translate: TranslateService
  ) {}

  @Effect()
  getDevices$ = this.actions$.pipe(
    ofType(DashboardActionType.getDevice),
    switchMap(() =>
      this.deviceService.getAllDevice().pipe(
        map(devices => new GetDevicesSuccess(devices)),
        catchError(error => of(new GetDevicesError({ error })))
      )
    )
  );
}
