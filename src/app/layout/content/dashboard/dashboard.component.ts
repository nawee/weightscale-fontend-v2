import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../sensor/service/data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(private dataService: DataService) {}
  stats$: Observable<any>;

  ngOnInit() {}
}
