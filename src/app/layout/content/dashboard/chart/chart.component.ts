import { Component, OnInit } from '@angular/core';
import { ReportRequest } from '../../sensor/model/reportRequest';
import { DataService } from '../../sensor/service/data.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  data: LineChartData[] = [];
  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    // this.getData();
  }

  getData() {
    // const request: ReportRequest = {
    //   type: 'temperature',
    //   deviceIds: ['5c4d24b95fe1dbe83bb632b3'],
    //   startDate: '2019-08-10',
    //   endDate: '2019-08-11'
    // };
    // this.dataService.getReport(request).subscribe(report => {
    //   this.data = report.data
    //     .map(obj => ({
    //       date: new Date(obj.createDate),
    //       value: obj.value
    //     }))
    //     .filter(obj => obj.value >= 0);
    //   console.log(this.data);
    // });
  }
}
