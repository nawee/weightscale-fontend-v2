import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Device } from '../../device/model/device';
import { Store } from '@ngrx/store';
import { DashboardState } from '../store/dashboard.reducer';
import {
  GetDevices,
  SelectDevices,
  SelectSensorType
} from '../store/dashboard.action';
import { getDevices, getLoading } from '../store/dashboard.selector';

@Component({
  selector: 'app-input-filter',
  templateUrl: './input-filter.component.html',
  styleUrls: ['./input-filter.component.scss']
})
export class InputFilterComponent implements OnInit {
  deviceSelectForm = new FormControl();
  devices$: Observable<Device[]>;
  selectedTime: string;
  sensorTypeSelectForm = new FormControl();
  sensorTypeList = [
    'weight',
    'temperature',
    'humidity',
    'pressure',
    'altitude',
    'light',
    'gas'
  ];

  constructor(private store: Store<DashboardState>) {}

  ngOnInit(): void {
    this.devices$ = this.store.select(getDevices);
    this.store.dispatch(new GetDevices());
  }

  selectSensorTypeChange(event) {
    if (!event) {
      this.store.dispatch(
        new SelectSensorType({
          sensorType: this.sensorTypeSelectForm.value
        })
      );
    }
  }

  selectDeviceChange(event) {
    if (!event) {
      this.store.dispatch(
        new SelectDevices({ devices: this.deviceSelectForm.value || [] })
      );
    }
  }

  refresh() {
    this.store.dispatch(
      new SelectDevices({ devices: this.deviceSelectForm.value || [] })
    );
  }
}
