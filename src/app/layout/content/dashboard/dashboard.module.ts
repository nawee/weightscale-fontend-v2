import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatTableModule,
  MatPaginatorModule,
  MatCardModule,
  MatSelectModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { StatusBoxModule } from 'src/app/shared/component/statusbox/statusBox.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.mudule';
import { ChartComponent } from './chart/chart.component';
import { InputFilterComponent } from './input-filter/input-filter.component';
import { LineChartModule } from 'src/app/shared/component/linechart/line-chart.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DashboardReducer } from './store/dashboard.reducer';
import { DashboardEffect } from './store/dashboard.effect';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatProgressBarModule,
    StatusBoxModule,
    FlexLayoutModule,
    DashboardRoutingModule,
    LineChartModule,
    StoreModule.forFeature('dashboard', DashboardReducer),
    EffectsModule.forFeature([DashboardEffect])
  ],
  declarations: [DashboardComponent, ChartComponent, InputFilterComponent]
})
export class DashboardModule {}
