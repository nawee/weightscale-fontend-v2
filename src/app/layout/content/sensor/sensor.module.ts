import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatTableModule,
  MatPaginatorModule,
  MatCardModule,
  MatSelectModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { SensorComponent } from './sensor.component';
import { SensorRoutingModule } from './sensor-routing.module';
import { DataListComponent } from './data-list/data-list.component';
import { ReportDialogComponent } from './report/report-dialog.component';
import { sensorReducer } from './store/sensor.reducer';
import { SensorEffect } from './store/sensor.effect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { StatusBoxModule } from 'src/app/shared/component/statusbox/statusBox.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DataFilterComponent } from './data-filter/data-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SensorRoutingModule,
    TranslateModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatProgressBarModule,
    StatusBoxModule,
    FlexLayoutModule,
    StoreModule.forFeature('sensor', sensorReducer),
    EffectsModule.forFeature([SensorEffect])
  ],
  declarations: [
    SensorComponent,
    DataListComponent,
    ReportDialogComponent,
    DataFilterComponent
  ],
  entryComponents: [ReportDialogComponent]
})
export class SensorModule {}
