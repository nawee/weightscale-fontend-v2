import { createFeatureSelector, createSelector } from '@ngrx/store';
import { SensorState } from './sensor.reducer';

export const getSensorState = createFeatureSelector<SensorState>('sensor');
export const getSensorType = createSelector(
  getSensorState,
  (state: SensorState) => state.sensorType
);
export const getPage = createSelector(
  getSensorState,
  (state: SensorState) => state.page
);
export const getSize = createSelector(
  getSensorState,
  (state: SensorState) => state.size
);
export const getTotal = createSelector(
  getSensorState,
  (state: SensorState) => state.total
);
export const getLoading = createSelector(
  getSensorState,
  (state: SensorState) => state.loading
);
export const getTableData = createSelector(
  getSensorState,
  (state: SensorState) => state.tableData
);
export const getDevices = createSelector(
  getSensorState,
  (state: SensorState) => state.devices
);
export const getSelectDevices = createSelector(
  getSensorState,
  (state: SensorState) => state.selectDevices
);
export const getStats = createSelector(
  getSensorState,
  (state: SensorState) => state.stats
);
