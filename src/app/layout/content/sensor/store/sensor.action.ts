import { Action } from '@ngrx/store';
import { DataResponse } from '../model/dataResponse';
import { Device } from '../../device/model/device';

export enum SensorActionType {
  selectSensorType = '[Sensor] select sensor type',
  changePagination = '[Sensor] change pagination',
  getData = '[Sensor] get data',
  getDataSuccess = '[Sensor] get data success',
  getDataError = '[Sensor] get data error',
  selectDevices = '[Sensor] select devices',
  getDevice = '[Sensor] get device',
  getDeviceSuccess = '[Sensor] get device success',
  getDeviceError = '[Sensor] get device error',
  getStats = '[Sensor] get stats',
  getStatsSuccess = '[Sensor] get stats success',
  getStatsError = '[Sensor] get stats error'
}
export class SelectSensorType implements Action {
  readonly type = SensorActionType.selectSensorType;
  constructor(public payload: { sensorType: string }) {}
}
export class ChangePagination implements Action {
  readonly type = SensorActionType.changePagination;
  constructor(public payload: { page: number; size: number }) {}
}
export class GetData implements Action {
  readonly type = SensorActionType.getData;
}
export class GetDataSuccess implements Action {
  readonly type = SensorActionType.getDataSuccess;
  constructor(public payload: { dataResponse: DataResponse }) {}
}
export class GetDataError implements Action {
  readonly type = SensorActionType.getDataError;

  constructor(public payload: { error: any }) {}
}
export class SelectDevices implements Action {
  readonly type = SensorActionType.selectDevices;
  constructor(public payload: { devices: Device[] }) {}
}
export class GetDevices implements Action {
  readonly type = SensorActionType.getDevice;
}
export class GetDevicesSuccess implements Action {
  readonly type = SensorActionType.getDeviceSuccess;
  constructor(public payload: Device[]) {}
}
export class GetDevicesError implements Action {
  readonly type = SensorActionType.getDeviceError;
  constructor(public payload: { error: any }) {}
}

export class GetStats implements Action {
  readonly type = SensorActionType.getStats;
}

export class GetStatsSuccess implements Action {
  readonly type = SensorActionType.getStatsSuccess;
  constructor(public payload: any) {}
}

export class GetStatsError implements Action {
  readonly type = SensorActionType.getStatsError;
  constructor(public payload: { error: any }) {}
}

export type SensorAction =
  | SelectSensorType
  | ChangePagination
  | GetData
  | GetDataSuccess
  | GetDataError
  | SelectDevices
  | GetDevices
  | GetDevicesSuccess
  | GetDevicesError
  | GetStats
  | GetStatsSuccess
  | GetStatsError;
