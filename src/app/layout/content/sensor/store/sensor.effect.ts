import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  SensorAction,
  SensorActionType,
  GetDataSuccess,
  GetData,
  GetDevicesSuccess,
  GetDevicesError,
  GetDataError,
  GetStatsSuccess,
  GetStatsError,
  GetStats
} from './sensor.action';
import {
  switchMap,
  withLatestFrom,
  map,
  catchError,
  mergeMap,
  tap
} from 'rxjs/operators';
import { DataService } from '../service/data.service';
import { Store } from '@ngrx/store';
import { SensorState } from './sensor.reducer';
import {
  getPage,
  getSize,
  getSensorType,
  getSelectDevices
} from './sensor.selector';
import { DataResponse } from '../model/dataResponse';
import { DeviceService } from '../../device/service/device.service';
import { of, merge } from 'rxjs';
import { LoadingShow, LoadingHide } from 'src/app/store/app.action';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
@Injectable()
export class SensorEffect {
  constructor(
    private actions$: Actions<SensorAction>,
    private dataService: DataService,
    private store: Store<SensorState>,
    private deviceService: DeviceService,
    private alert: ToastrService,
    private translate: TranslateService
  ) {}

  @Effect()
  getData$ = this.actions$.pipe(
    ofType(SensorActionType.getData),
    withLatestFrom(
      this.store.select(getPage),
      this.store.select(getSize),
      this.store.select(getSensorType),
      this.store.select(getSelectDevices)
    ),
    map(([action, page, size, sensorType, selectDevices]) => {
      return {
        page,
        size,
        sensorType,
        selectDevices: selectDevices.map(devices => devices.id.toString())
      };
    }),
    switchMap(({ page, size, sensorType, selectDevices }) =>
      merge(
        of(new LoadingShow()),
        of(new GetStats()),
        this.dataService
          .getData({
            page: page,
            limit: size,
            type: sensorType,
            deviceIds: selectDevices
          })
          .pipe(
            mergeMap((dataResponse: DataResponse) => [
              new LoadingHide(),
              new GetDataSuccess({ dataResponse })
            ]),
            catchError(error => of(new GetDataError({ error })))
          )
      )
    )
  );

  @Effect()
  selectSensorType$ = this.actions$.pipe(
    ofType(SensorActionType.selectSensorType),
    map(() => new GetData())
  );

  @Effect()
  changePagination$ = this.actions$.pipe(
    ofType(SensorActionType.changePagination),
    map(() => new GetData())
  );

  @Effect()
  getDevices$ = this.actions$.pipe(
    ofType(SensorActionType.getDevice),
    switchMap(() =>
      this.deviceService.getAllDevice().pipe(
        map(devices => new GetDevicesSuccess(devices)),
        catchError(error => of(new GetDevicesError({ error })))
      )
    )
  );

  @Effect()
  selectDevices$ = this.actions$.pipe(
    ofType(SensorActionType.selectDevices),
    map(() => new GetData())
  );

  @Effect()
  error$ = this.actions$.pipe(
    ofType(SensorActionType.getDataError, SensorActionType.getDeviceError),
    map(action => action.payload.error),
    tap(error => {
      this.alert.error(
        error.responseMessage,
        this.translate.instant('lb.errormessage')
      );
    }),
    map(() => new LoadingHide())
  );

  @Effect()
  getStats$ = this.actions$.pipe(
    ofType(SensorActionType.getStats),
    withLatestFrom(
      this.store.select(getSensorType),
      this.store.select(getSelectDevices)
    ),
    map(([action, sensorType, selectDevices]) => {
      return {
        sensorType,
        selectDevices: selectDevices.map(devices => devices.id.toString())
      };
    }),
    switchMap(({ sensorType, selectDevices }) =>
      this.dataService
        .getStats({
          type: sensorType,
          deviceIds: selectDevices,
          startDate: new Date(new Date().setHours(0, 0, 0)).toISOString(),
          endDate: new Date(new Date().setHours(23, 59, 59)).toISOString()
        })
        .pipe(
          map(stats => new GetStatsSuccess(stats)),
          catchError(error => of(new GetStatsError({ error })))
        )
    )
  );
}
