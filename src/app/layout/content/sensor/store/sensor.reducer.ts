import { AppState } from 'src/app/store/app.reducer';
import { SensorAction, SensorActionType } from './sensor.action';
import { TableData } from '../model/tableData';
import { Device } from '../../device/model/device';

export interface AppSensorState extends AppState {
  sensorState: SensorState;
}
export interface SensorState {
  sensorType: string;
  page: number;
  size: number;
  total: number;
  loading: boolean;
  tableData: TableData[];
  devices: Device[];
  selectDevices: Device[];
  error?: any;
  stats?: { avg: string; min: string; max: string };
}

export const initialSensorState: SensorState = {
  sensorType: '',
  page: 1,
  size: 100,
  total: 0,
  loading: false,
  tableData: [],
  devices: [],
  selectDevices: []
};

export function sensorReducer(
  state = initialSensorState,
  action: SensorAction
): SensorState {
  switch (action.type) {
    case SensorActionType.changePagination: {
      return { ...state, page: action.payload.page, size: action.payload.size };
    }
    case SensorActionType.selectSensorType: {
      return {
        ...state,
        sensorType: action.payload.sensorType
      };
    }
    case SensorActionType.getData: {
      return state;
    }
    case SensorActionType.getDataSuccess: {
      return {
        ...state,
        total: action.payload.dataResponse.data.total,
        tableData: action.payload.dataResponse.data.docs
      };
    }
    case SensorActionType.getDataError: {
      return { ...state, error: action.payload.error };
    }
    case SensorActionType.getDevice: {
      return state;
    }
    case SensorActionType.getDeviceSuccess: {
      return { ...state, devices: action.payload };
    }
    case SensorActionType.getDeviceError: {
      return state;
    }
    case SensorActionType.selectDevices: {
      return { ...state, selectDevices: action.payload.devices };
    }
    case SensorActionType.getStats: {
      return state;
    }
    case SensorActionType.getStatsSuccess: {
      return {
        ...state,
        stats: {
          avg: action.payload.avg,
          min: action.payload.min,
          max: action.payload.max
        }
      };
    }
    case SensorActionType.getStatsError: {
      return {
        ...state,
        stats: {
          avg: '-',
          min: '-',
          max: '-'
        },
        error: action.payload.error
      };
    }
    default:
      return state;
  }
}
