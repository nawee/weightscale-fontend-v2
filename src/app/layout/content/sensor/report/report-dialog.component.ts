import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { SensorState } from '../store/sensor.reducer';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Device } from '../../device/model/device';
import { getDevices } from '../store/sensor.selector';
import { FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-report-dialog',
  templateUrl: './report-dialog.component.html',
  styleUrls: ['./report-dialog.component.scss']
})
export class ReportDialogComponent implements OnInit {
  public confirmMessage: string;
  devices$: Observable<Device[]>;
  deviceSelectForm = new FormControl('', [Validators.required]);
  startDate = new FormControl('', [Validators.required]);
  endDate = new FormControl('', [Validators.required]);

  constructor(
    private store: Store<SensorState>,
    public dialogRef: MatDialogRef<ReportDialogComponent>
  ) {}

  ngOnInit(): void {
    this.devices$ = this.store.select(getDevices);
  }

  download() {
    if (this.validate()) {
      const startDate = moment(this.startDate.value)
        .startOf('day')
        .toISOString();
      const endDate = moment(this.endDate.value)
        .endOf('day')
        .toISOString();

      this.dialogRef.close({
        deviceId: this.deviceSelectForm.value,
        startDate,
        endDate
      });
    }
  }

  private validate() {
    if (
      this.deviceSelectForm.valid &&
      this.startDate.valid &&
      this.endDate.valid
    ) {
      return true;
    }
    return false;
  }
}
