import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatDialog } from '@angular/material';
import { TableData } from '../model/tableData';
import { Store } from '@ngrx/store';
import { SensorState } from '../store/sensor.reducer';
import {
  getSensorType,
  getTotal,
  getTableData,
  getPage,
  getSize
} from '../store/sensor.selector';
import { Observable } from 'rxjs';
import { ChangePagination, SelectDevices } from '../store/sensor.action';
import { ReportDialogComponent } from '../report/report-dialog.component';
import { DataService } from '../service/data.service';
import { ReportRequest } from '../model/reportRequest';
import { ExcelService } from '../service/excel.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['no', 'time', 'deviceName', 'value'];
  data: TableData[] = [];
  tableData$: Observable<TableData[]>;
  sensorType$: Observable<string>;
  total$: Observable<number>;
  page$: Observable<number>;
  size$: Observable<number>;
  sensorType: string;
  loading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private store: Store<SensorState>,
    private dialog: MatDialog,
    private dataService: DataService,
    private excelService: ExcelService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    this.sensorType$ = this.store.select(getSensorType);
    this.total$ = this.store.select(getTotal);
    this.tableData$ = this.store.select(getTableData);
    this.page$ = this.store.select(getPage);
    this.size$ = this.store.select(getSize);

    this.sensorType$.subscribe(type => (this.sensorType = type));
  }

  ngAfterViewInit(): void {
    this.paginator.page.subscribe(
      (pagination: { pageIndex: number; pageSize: number }) =>
        this.store.dispatch(
          new ChangePagination({
            page: pagination.pageIndex + 1,
            size: pagination.pageSize
          })
        )
    );
  }

  report() {
    const dialogRef = this.dialog.open(ReportDialogComponent, {
      width: '350px',
      disableClose: false
    });
    dialogRef.componentInstance.confirmMessage =
      'Are you sure you want download report as xlsx?';

    dialogRef.beforeClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        const request: ReportRequest = {
          type: this.sensorType,
          deviceIds: [result.deviceId.id],
          startDate: result.startDate,
          endDate: result.endDate
        };
        this.dataService.getReport(request).subscribe(report => {
          this.loading = false;
          this.excelService.exportAsExcelFile(report.data, this.sensorType);
        });
      }
    });
  }

  translateUnit(sensorUnit: string) {
    switch (sensorUnit) {
      case 'weight':
        return this.translate.instant('lb.weightUnit');
      case 'temperature':
        return this.translate.instant('lb.tempUnit');
      case 'humidity':
        return this.translate.instant('lb.humidUnit');
      case 'pressure':
        return this.translate.instant('lb.pressureUnit');
      case 'altitude':
        return this.translate.instant('lb.altitudeUnit');
      case 'light':
        return this.translate.instant('lb.lightUnit');
      case 'gas':
        return this.translate.instant('lb.gasUnit');
      default:
        return 'value';
    }
  }
}
