import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { SensorState } from './store/sensor.reducer';
import { SelectSensorType, GetDevices } from './store/sensor.action';
import { Observable } from 'rxjs';
import { getStats } from './store/sensor.selector';

@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.scss']
})
export class SensorComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private store: Store<SensorState>
  ) {}

  stats$: Observable<any>;

  ngOnInit() {
    this.subscribeParams();
    this.stats$ = this.store.select(getStats);
    this.store.dispatch(new GetDevices());
  }

  private subscribeParams() {
    this.route.params.subscribe(params =>
      this.store.dispatch(
        new SelectSensorType({ sensorType: params.sensorType })
      )
    );
  }
}
