import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConfigService } from '../../../../shared/services/config/config.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataResponse } from '../model/dataResponse';
import { DataRequest } from '../model/dataRequest';
import { ReportRequest } from '../model/reportRequest';
import { ReportResponse } from '../model/reportResponse';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  getData(request: DataRequest): Observable<DataResponse> {
    return this.http.post<DataResponse>(`${this.config.getApiUrl()}/data`, {
      page: request.page.toString(),
      limit: request.limit.toString(),
      type: request.type,
      deviceIds: request.deviceIds
    });
  }

  getReport(request: ReportRequest): Observable<ReportResponse> {
    const { type, deviceIds, startDate, endDate } = request;
    return this.http.post<ReportResponse>(
      `${this.config.getApiUrl()}/data/report`,
      {
        type,
        deviceIds,
        startDate,
        endDate
      }
    );
  }

  getStats(request: ReportRequest): Observable<any> {
    const { type, deviceIds, startDate, endDate } = request;
    return this.http
      .post(`${this.config.getApiUrl()}/data/stats`, {
        type,
        deviceIds,
        startDate,
        endDate
      })
      .pipe(
        map((resp: any) => ({
          avg: resp.data[0].avg,
          min: resp.data[0].min,
          max: resp.data[0].max
        }))
      );
  }
}
