export interface TableData {
  id: string;
  type: string;
  value: string;
  device: { id: string; name: string; topic: string };
  createDate: string;
}
