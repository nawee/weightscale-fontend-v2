export interface DataResponse {
  data: {
    total: number;
    limit: number;
    page: number;
    pages: number;
    docs: Array<{
      id: string;
      type: string;
      value: string;
      device: { id: string; name: string; topic: string };
      createDate: string;
    }>;
  };
}
