export interface DataRequest {
  page: number;
  limit: number;
  type: string;
  deviceIds: string[];
}
