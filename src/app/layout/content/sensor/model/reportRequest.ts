export interface ReportRequest {
  type: string;
  deviceIds: string[];
  startDate: string;
  endDate: string;
}
