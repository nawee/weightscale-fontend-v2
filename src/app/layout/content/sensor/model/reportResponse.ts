export interface ReportResponse {
  data: [
    {
      type: string;
      value: number;
      device: { id: string; name: string; topic: string };
      createDate: string;
    }
  ];
}
