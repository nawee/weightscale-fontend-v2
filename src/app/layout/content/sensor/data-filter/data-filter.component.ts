import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { SensorState } from '../store/sensor.reducer';
import { SelectDevices } from '../store/sensor.action';
import { Observable } from 'rxjs';
import { Device } from '../../device/model/device';
import { getDevices, getSensorType } from '../store/sensor.selector';

@Component({
  selector: 'app-data-filter',
  templateUrl: './data-filter.component.html',
  styleUrls: ['./data-filter.component.scss']
})
export class DataFilterComponent implements OnInit {
  deviceSelectForm = new FormControl();
  devices$: Observable<Device[]>;
  sensorType$: Observable<string>;
  selectedTime: String;

  constructor(private store: Store<SensorState>) {}

  ngOnInit(): void {
    this.sensorType$ = this.store.select(getSensorType);
    this.devices$ = this.store.select(getDevices);
  }

  selectChange(event) {
    if (!event) {
      this.store.dispatch(
        new SelectDevices({ devices: this.deviceSelectForm.value || [] })
      );
    }
  }

  refresh() {
    this.store.dispatch(
      new SelectDevices({ devices: this.deviceSelectForm.value || [] })
    );
  }
}
