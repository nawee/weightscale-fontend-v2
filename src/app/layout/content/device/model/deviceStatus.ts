export interface DeviceStatus {
  id: number;
  value: string;
  timestamp: string;
}
