import { DeviceStatus } from './deviceStatus';

export interface Device {
  id?: number;
  name: string;
  topic: string;
  deviceStatus?: DeviceStatus;
  mini?: string;
  diff?: string;
  sta?: string;
}
