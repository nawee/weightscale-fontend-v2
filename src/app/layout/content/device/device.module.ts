import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatCardModule,
  MatExpansionModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { DeviceComponent } from './device.component';
import { DeviceRoutingModule } from './device-routing.module';
import { DeviceListComponent } from './device-list/device-list.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { deviceReducer } from './store/device.reducer';
import { EffectsModule } from '@ngrx/effects';
import { DeviceEffect } from './store/device.effect';
import { DeviceCreateComponent } from './device-create/device-create.component';
import { ConfirmDialogModule } from 'src/app/shared/component/dialog/confirm-dialog.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    DeviceRoutingModule,
    MatCardModule,
    MatExpansionModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    ConfirmDialogModule,
    StoreModule.forFeature('devices', deviceReducer),
    EffectsModule.forFeature([DeviceEffect])
  ],
  declarations: [DeviceComponent, DeviceListComponent, DeviceCreateComponent],
  entryComponents: [DeviceCreateComponent]
})
export class DeviceModule {}
