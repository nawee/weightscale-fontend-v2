import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Device } from '../model/device';

@Component({
  selector: 'app-device-create',
  templateUrl: './device-create.component.html',
  styleUrls: ['./device-create.component.scss']
})
export class DeviceCreateComponent implements OnInit {
  device: Device;

  constructor(public dialogRef: MatDialogRef<DeviceCreateComponent>) {
    this.device = {
      name: '',
      topic: ''
    };
  }

  ngOnInit() {}

  cancel() {
    this.dialogRef.close();
  }

  create() {
    this.dialogRef.close(this.device);
  }
}
