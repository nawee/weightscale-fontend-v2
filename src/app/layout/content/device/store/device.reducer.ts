import { Device } from '../model/device';
import { DeviceAction, DeviceActionType } from './device.action';
import { AppState } from 'src/app/store/app.reducer';
import { DeviceStatus } from '../model/deviceStatus';

export interface AppDeviceState extends AppState {
  deviceState: DeviceState;
}
export interface DeviceState {
  devices?: Device[];
  error?: any;
}

export const initialDeviceState: DeviceState = {
  devices: []
};

export function deviceReducer(
  state = initialDeviceState,
  action: DeviceAction
): DeviceState {
  switch (action.type) {
    case DeviceActionType.getDevice: {
      return state;
    }
    case DeviceActionType.getDeviceSuccess: {
      return {
        ...state,
        devices: action.payload
      };
    }
    case DeviceActionType.getDeviceError: {
      return state;
    }
    case DeviceActionType.updateDevice: {
      return state;
    }
    case DeviceActionType.updateDeviceSuccess: {
      return state;
    }
    case DeviceActionType.updateDeviceError: {
      return { ...state, error: action.payload.error };
    }
    case DeviceActionType.removeDevice: {
      return state;
    }
    case DeviceActionType.removeDeviceSuccess: {
      return state;
    }
    case DeviceActionType.removeDeviceError: {
      return { ...state, error: action.payload.error };
    }
    case DeviceActionType.createDevice: {
      return state;
    }
    case DeviceActionType.refreshStatusDeviceError: {
      return { ...state, error: action.payload.error };
    }
    case DeviceActionType.getStatusDeviceError: {
      return { ...state, error: action.payload.error };
    }
    case DeviceActionType.getStatusDeviceSuccess: {
      return {
        ...state,
        devices: updateDeviceState(state.devices, action.payload.deviceStatus)
      };
    }
    default:
      return state;
  }
}
function updateDeviceState(
  devices: Device[],
  deviceStatus: DeviceStatus
): Device[] {
  const index = devices.findIndex(device => device.id === deviceStatus.id);
  const deviceUpdate = devices[index];
  const newDevices = [
    ...devices.slice(0, index),
    {
      ...deviceUpdate,
      mini: deviceStatus.value.split(',')[1],
      diff: deviceStatus.value.split(',')[2],
      sta: deviceStatus.value.split(',')[3],
      deviceStatus
    },
    ...devices.slice(index + 1)
  ];
  return newDevices;
}
