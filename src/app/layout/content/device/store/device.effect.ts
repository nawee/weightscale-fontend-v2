import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {
  DeviceAction,
  DeviceActionType,
  GetDevicesError,
  GetDevicesSuccess,
  UpdateDeviceSuccess,
  UpdateDeviceError,
  GetDevices,
  RemoveDeviceSuccess,
  RemoveDeviceError,
  CreateDeviceSuccess,
  CreateDeviceError,
  GetStatusDeviceError,
  GetStatusDeviceSuccess,
  SetStaDeviceSuccess,
  SetDiffDeviceSuccess,
  SetMiniDeviceSuccess,
  RefreshStatusDeviceSuccess,
  RefreshStatusDeviceError,
  GetStatusDevice,
  RefreshStatusDevice,
  ResetDeviceSuccess,
  ResetDeviceError
} from './device.action';
import {
  map,
  switchMap,
  catchError,
  tap,
  mergeMap,
  exhaustMap,
  delay
} from 'rxjs/operators';
import { DeviceService } from '../service/device.service';
import { of, from } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { LoadingHide } from 'src/app/store/app.action';

@Injectable()
export class DeviceEffect {
  constructor(
    private actions$: Actions<DeviceAction>,
    private deviceService: DeviceService,
    private alert: ToastrService,
    private translate: TranslateService
  ) {}

  @Effect()
  getDevices$ = this.actions$.pipe(
    ofType(DeviceActionType.getDevice),
    switchMap(() =>
      this.deviceService.getAllDevice().pipe(
        map(devices => new GetDevicesSuccess(devices)),
        catchError(error => of(new GetDevicesError({ error })))
      )
    )
  );

  @Effect()
  GetDevicesSuccess$ = this.actions$.pipe(
    ofType(DeviceActionType.getDeviceSuccess),
    mergeMap(action => action.payload),
    map(device => new RefreshStatusDevice(device))
  );

  @Effect()
  updateDevices$ = this.actions$.pipe(
    ofType(DeviceActionType.updateDevice),
    map(action => action.payload),
    switchMap(payload =>
      this.deviceService
        .update(payload.id, {
          name: payload.device.name,
          topic: payload.device.topic
        })
        .pipe(
          mergeMap(device => [
            new UpdateDeviceSuccess(device),
            new GetDevices()
          ]),
          catchError(error => of(new UpdateDeviceError({ error })))
        )
    )
  );

  @Effect()
  removeDevices$ = this.actions$.pipe(
    ofType(DeviceActionType.removeDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.remove(payload).pipe(
        mergeMap(() => [new RemoveDeviceSuccess(), new GetDevices()]),
        catchError(error => of(new RemoveDeviceError({ error })))
      )
    )
  );

  @Effect()
  createDevices$ = this.actions$.pipe(
    ofType(DeviceActionType.createDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.create(payload).pipe(
        mergeMap(device => [new CreateDeviceSuccess(device), new GetDevices()]),
        catchError(error => of(new CreateDeviceError({ error })))
      )
    )
  );

  @Effect()
  getStatusDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.getStatusDevice),
    map(action => action.payload),
    mergeMap(payload =>
      this.deviceService.getStatus(payload.id).pipe(
        map(
          deviceStatus =>
            new GetStatusDeviceSuccess({
              deviceStatus
            })
        ),
        catchError(error => of(new GetStatusDeviceError({ error })))
      )
    )
  );

  @Effect()
  refreshStatusDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.refreshStatusDevice),
    map(action => action.payload),
    mergeMap(payload =>
      this.deviceService.refreshStatus(payload.topic).pipe(
        delay(1500),
        mergeMap(() => [
          new RefreshStatusDeviceSuccess(),
          new GetStatusDevice(payload)
        ]),
        catchError(error => of(new RefreshStatusDeviceError({ error })))
      )
    )
  );

  @Effect()
  setMiniDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.setMiniDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.setMini(payload.topic, payload.mini).pipe(
        mergeMap(() => [
          new SetMiniDeviceSuccess(),
          new RefreshStatusDevice(payload)
        ]),
        catchError(error => of(new GetStatusDeviceError({ error })))
      )
    )
  );

  @Effect()
  setDiffDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.setDiffDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.setDiff(payload.topic, payload.diff).pipe(
        mergeMap(() => [
          new SetDiffDeviceSuccess(),
          new RefreshStatusDevice(payload)
        ]),
        catchError(error => of(new GetStatusDeviceError({ error })))
      )
    )
  );

  @Effect()
  setStaDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.setStaDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.setSta(payload.topic, payload.sta).pipe(
        mergeMap(() => [
          new SetStaDeviceSuccess(),
          new RefreshStatusDevice(payload)
        ]),
        catchError(error => of(new GetStatusDeviceError({ error })))
      )
    )
  );

  @Effect()
  resetDevice$ = this.actions$.pipe(
    ofType(DeviceActionType.resetDevice),
    map(action => action.payload),
    exhaustMap(payload =>
      this.deviceService.reboot(payload).pipe(
        map(() => new ResetDeviceSuccess()),
        catchError(error => of(new ResetDeviceError({ error })))
      )
    )
  );

  @Effect({ dispatch: false })
  success$ = this.actions$.pipe(
    ofType(
      DeviceActionType.updateDeviceSuccess,
      DeviceActionType.removeDeviceSuccess,
      DeviceActionType.resetDeviceSuccess,
      DeviceActionType.setMiniDeviceSuccess,
      DeviceActionType.setDiffDeviceSuccess,
      DeviceActionType.setStaDeviceSucces,
      DeviceActionType.removeDeviceSuccess
    ),
    tap(() => {
      this.alert.success('success', 'Success');
    })
  );

  @Effect()
  error$ = this.actions$.pipe(
    ofType(
      DeviceActionType.getDeviceError,
      DeviceActionType.removeDeviceError,
      DeviceActionType.setDiffDeviceError,
      DeviceActionType.setStaDeviceError,
      DeviceActionType.setMiniDeviceError
    ),
    map(action => action.payload.error),
    tap(error => {
      this.alert.error(
        error.responseMessage,
        this.translate.instant('lb.errormessage')
      );
    }),
    map(() => new LoadingHide())
  );
}
