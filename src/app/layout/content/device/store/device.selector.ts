import { DeviceState } from './device.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getDeviceState = createFeatureSelector<DeviceState>('devices');
export const getDevices = createSelector(
  getDeviceState,
  (state: DeviceState) => state.devices
);
