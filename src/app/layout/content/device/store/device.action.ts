import { Action } from '@ngrx/store';
import { Device } from '../model/device';
import { DeviceStatus } from '../model/deviceStatus';

export enum DeviceActionType {
  getDevice = '[Device] get device',
  getDeviceSuccess = '[Device] get device success',
  getDeviceError = '[Device] get device error',
  createDevice = '[Device] create device',
  createDeviceSuccess = '[Device] create device success]',
  createDeviceError = '[Device] create device error]',
  removeDevice = '[Device] remove device',
  removeDeviceSuccess = '[Device] remove device success',
  removeDeviceError = '[Device] remove device error',
  updateDevice = '[Device] update device',
  updateDeviceSuccess = '[Device] update device success',
  updateDeviceError = '[Device] update device error',
  resetDevice = '[Device] reset device',
  resetDeviceSuccess = '[Device] reset device success',
  resetDeviceError = '[Device] reset device error',
  setDiffDevice = '[Device] set diff device',
  setDiffDeviceSuccess = '[Device] set diff device success',
  setDiffDeviceError = '[Devide] set diff device error',
  setStaDevice = '[Device] set sta device',
  setStaDeviceSucces = '[Device] set sta device success',
  setStaDeviceError = '[Device] set sta device error',
  setMiniDevice = '[Device] set mini device',
  setMiniDeviceSuccess = '[Device] set mini device success',
  setMiniDeviceError = '[Device] set mini device error',
  getStatusDevice = '[Device] get status device',
  getStatusDeviceSuccess = '[Device] get status device success',
  getStatusDeviceError = '[Device] get status device error',
  refreshStatusDevice = '[Device] refresh status device',
  refreshStatusDeviceSuccess = '[Device] refresh status device success',
  refreshStatusDeviceError = '[Device] refresh status device error'
}

export class GetDevices implements Action {
  readonly type = DeviceActionType.getDevice;
}
export class GetDevicesSuccess implements Action {
  readonly type = DeviceActionType.getDeviceSuccess;
  constructor(public payload: Device[]) {}
}
export class GetDevicesError implements Action {
  readonly type = DeviceActionType.getDeviceError;
  constructor(public payload: { error: any }) {}
}
export class CreateDevice implements Action {
  readonly type = DeviceActionType.createDevice;
  constructor(public payload: Device) {}
}
export class CreateDeviceSuccess implements Action {
  readonly type = DeviceActionType.createDeviceSuccess;
  constructor(public payload: Device) {}
}
export class CreateDeviceError implements Action {
  readonly type = DeviceActionType.createDeviceError;
  constructor(public payload: { error: any }) {}
}
export class RemoveDevice implements Action {
  readonly type = DeviceActionType.removeDevice;
  constructor(public payload: number) {}
}
export class RemoveDeviceSuccess implements Action {
  readonly type = DeviceActionType.removeDeviceSuccess;
}
export class RemoveDeviceError implements Action {
  readonly type = DeviceActionType.removeDeviceError;
  constructor(public payload: { error: any }) {}
}
export class UpdateDevice implements Action {
  readonly type = DeviceActionType.updateDevice;
  constructor(public payload: { id: number; device: Device }) {}
}
export class UpdateDeviceSuccess implements Action {
  readonly type = DeviceActionType.updateDeviceSuccess;
  constructor(public payload: Device) {}
}
export class UpdateDeviceError implements Action {
  readonly type = DeviceActionType.updateDeviceError;
  constructor(public payload: { error: any }) {}
}
export class ResetDevice implements Action {
  readonly type = DeviceActionType.resetDevice;
  constructor(public payload: Device) {}
}
export class ResetDeviceSuccess implements Action {
  readonly type = DeviceActionType.resetDeviceSuccess;
}
export class ResetDeviceError implements Action {
  readonly type = DeviceActionType.resetDeviceError;
  constructor(public payload: { error: any }) {}
}
export class SetMiniDevice implements Action {
  readonly type = DeviceActionType.setMiniDevice;
  constructor(public payload: Device) {}
}
export class SetMiniDeviceSuccess implements Action {
  readonly type = DeviceActionType.setMiniDeviceSuccess;
}
export class SetMiniDeviceError implements Action {
  readonly type = DeviceActionType.setMiniDeviceError;
  constructor(public payload: { error: any }) {}
}
export class SetDiffDevice implements Action {
  readonly type = DeviceActionType.setDiffDevice;
  constructor(public payload: Device) {}
}
export class SetDiffDeviceSuccess implements Action {
  readonly type = DeviceActionType.setDiffDeviceSuccess;
}
export class SetDiffDeviceError implements Action {
  readonly type = DeviceActionType.setDiffDeviceError;
  constructor(public payload: { error: any }) {}
}
export class SetStaDevice implements Action {
  readonly type = DeviceActionType.setStaDevice;
  constructor(public payload: Device) {}
}
export class SetStaDeviceSuccess implements Action {
  readonly type = DeviceActionType.setStaDeviceSucces;
}
export class SetStaDeviceError implements Action {
  readonly type = DeviceActionType.setStaDeviceError;
  constructor(public payload: { error: any }) {}
}
export class GetStatusDevice implements Action {
  readonly type = DeviceActionType.getStatusDevice;
  constructor(public payload: Device) {}
}
export class GetStatusDeviceSuccess implements Action {
  readonly type = DeviceActionType.getStatusDeviceSuccess;
  constructor(public payload: { deviceStatus: DeviceStatus }) {}
}
export class GetStatusDeviceError implements Action {
  readonly type = DeviceActionType.getStatusDeviceError;
  constructor(public payload: { error: any }) {}
}
export class RefreshStatusDevice implements Action {
  readonly type = DeviceActionType.refreshStatusDevice;
  constructor(public payload: Device) {}
}
export class RefreshStatusDeviceSuccess implements Action {
  readonly type = DeviceActionType.refreshStatusDeviceSuccess;
}
export class RefreshStatusDeviceError implements Action {
  readonly type = DeviceActionType.refreshStatusDeviceError;
  constructor(public payload: { error: any }) {}
}
export type DeviceAction =
  | GetDevices
  | GetDevicesSuccess
  | GetDevicesError
  | CreateDevice
  | CreateDeviceSuccess
  | CreateDeviceError
  | RemoveDevice
  | RemoveDeviceSuccess
  | RemoveDeviceError
  | UpdateDevice
  | UpdateDeviceSuccess
  | UpdateDeviceError
  | ResetDevice
  | ResetDeviceSuccess
  | ResetDeviceError
  | SetMiniDevice
  | SetMiniDeviceSuccess
  | SetMiniDeviceError
  | SetDiffDevice
  | SetDiffDeviceSuccess
  | SetDiffDeviceError
  | SetStaDevice
  | SetStaDeviceSuccess
  | SetStaDeviceError
  | GetStatusDevice
  | GetStatusDeviceSuccess
  | GetStatusDeviceError
  | RefreshStatusDevice
  | RefreshStatusDeviceSuccess
  | RefreshStatusDeviceError;
