import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Device } from './model/device';
import {
  GetDevices,
  UpdateDevice,
  RemoveDevice,
  CreateDevice,
  GetStatusDevice,
  SetStaDevice,
  SetMiniDevice,
  SetDiffDevice,
  ResetDevice,
  RefreshStatusDevice
} from './store/device.action';
import { DeviceState } from './store/device.reducer';
import { Store } from '@ngrx/store';
import { getDevices } from './store/device.selector';
import { map, tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { DeviceCreateComponent } from './device-create/device-create.component';
import { ConfirmDialogComponent } from 'src/app/shared/component/dialog/confirm-dialog.component';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
  devices$: Observable<Device[]>;

  constructor(private store: Store<DeviceState>, private dialog: MatDialog) {}

  ngOnInit() {
    this.devices$ = this.store
      .select(getDevices)
      .pipe(map(devices => devices.map(device => ({ ...device }))));
    this.store.dispatch(new GetDevices());
  }

  update(device: Device) {
    this.store.dispatch(new UpdateDevice({ id: device.id, device }));
  }

  remove(device: Device) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      disableClose: false
    });
    dialogRef.componentInstance.confirmMessage =
      'Are you sure you want to delete?';

    dialogRef.afterClosed().subscribe((result: Boolean) => {
      if (result) {
        this.store.dispatch(new RemoveDevice(device.id));
      }
    });
  }

  reset(device: Device) {
    this.store.dispatch(new ResetDevice(device));
  }

  refreshStatus(device: Device) {
    this.store.dispatch(new RefreshStatusDevice(device));
  }

  mini(device: Device) {
    this.store.dispatch(new SetMiniDevice(device));
  }

  diff(device: Device) {
    this.store.dispatch(new SetDiffDevice(device));
  }

  sta(device: Device) {
    console.log(device);
    this.store.dispatch(new SetStaDevice(device));
  }

  create() {
    const dialogRef = this.dialog.open(DeviceCreateComponent, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe((result: Device) => {
      if (result) {
        this.store.dispatch(new CreateDevice(result));
      }
    });
  }
}
