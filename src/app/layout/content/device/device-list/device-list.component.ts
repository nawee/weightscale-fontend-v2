import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Device } from '../model/device';

@Component({
  selector: 'app-device-list',
  templateUrl: './device-list.component.html',
  styleUrls: ['./device-list.component.scss']
})
export class DeviceListComponent implements OnInit {
  @Input() devices: Device[];
  @Output() remove = new EventEmitter<Device>();
  @Output() update = new EventEmitter<Device>();
  @Output() reset = new EventEmitter<Device>();
  @Output() refreshStatus = new EventEmitter<Device>();
  @Output() mini = new EventEmitter<Device>();
  @Output() diff = new EventEmitter<Device>();
  @Output() sta = new EventEmitter<Device>();
  @Output() create = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  removeDevice(device: Device) {
    this.remove.emit({ ...device });
  }

  updateDevice(device: Device) {
    this.update.emit({ ...device });
  }

  miniDevice(device: Device) {
    this.mini.emit({ ...device });
  }

  diffDevice(device: Device) {
    this.diff.emit({ ...device });
  }

  staDevice(device: Device) {
    this.sta.emit({ ...device });
  }

  resetDevice(device: Device) {
    this.reset.emit({ ...device });
  }

  updateDeviceStatus(device: Device) {
    this.refreshStatus.emit({ ...device });
  }

  createDevice() {
    this.create.emit();
  }
}
