import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../../../shared/services/config/config.service';
import { Observable } from 'rxjs';
import { Device } from '../model/device';
import { map, tap } from 'rxjs/operators';
import { DeviceStatus } from '../model/deviceStatus';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  getAllDevice(): Observable<Device[]> {
    return this.http.get(this.config.getApiUrl() + '/device').pipe(
      map((resp: any) => <Device[]>resp.devices),
      map(devices => devices.sort(this.sortDeviceByName))
    );
  }

  update(id, params): Observable<Device> {
    return this.http
      .patch(`${this.config.getApiUrl()}/device/${id}`, params)
      .pipe(map((resp: any) => <Device>resp.device));
  }

  remove(id): Observable<any> {
    return this.http.delete(`${this.config.getApiUrl()}/device/${id}`);
  }

  create(device: Device): Observable<any> {
    return this.http.post(`${this.config.getApiUrl()}/device`, {
      name: device.name,
      topic: device.topic
    });
  }

  setMini(topic, value): Observable<any> {
    console.log('set mini', value);
    return this.http.get(`${this.config.getApiUrl()}/device/mini`, {
      params: { topic, value }
    });
  }

  setDiff(topic, value): Observable<any> {
    console.log('set diff', value);
    return this.http.get(`${this.config.getApiUrl()}/device/diff`, {
      params: { topic, value }
    });
  }

  setSta(topic, value): Observable<any> {
    console.log('set sta', value);
    return this.http.get(`${this.config.getApiUrl()}/device/sta`, {
      params: { topic, value }
    });
  }

  reboot(topic): Observable<any> {
    return this.http.get(`${this.config.getApiUrl()}/device/reboot`, {
      params: { topic }
    });
  }

  refreshStatus(topic): Observable<any> {
    return this.http.get(`${this.config.getApiUrl()}/device/refreshStatus`, {
      params: { topic }
    });
  }

  getStatus(id): Observable<DeviceStatus> {
    return this.http
      .get(`${this.config.getApiUrl()}/data/deviceStatus/${id}`)
      .pipe(
        map((resp: any) => {
          return {
            id: resp.data.device,
            timestamp: resp.data.createDate,
            value: resp.data.value
          };
        })
      );
  }

  sortDeviceByName = (a: Device, b: Device) => {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  }
}
