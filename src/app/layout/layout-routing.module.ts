import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './content/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'device',
        loadChildren: './content/device/device.module#DeviceModule'
      },
      {
        path: 'sensor',
        loadChildren: './content/sensor/sensor.module#SensorModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {}
