import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from './app.reducer';

export const getAppState = createFeatureSelector<AppState>('app');
export const getUser = createSelector(
  getAppState,
  state => state.user
);
export const getLoading = createSelector(
  getAppState,
  state => state.loading
);
export const getError = createSelector(
  getAppState,
  state => state.error
);
