import { ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { AppAction, AppActionType } from './app.action';
import { storeFreeze } from 'ngrx-store-freeze';
import { User } from '../shared/model/user';

export interface RootState {
  app: AppState;
}

export interface AppState {
  loading: boolean;
  loadingCtr: number;
  user: User;
  error?: any;
}

export const initialAppState: AppState = {
  loading: false,
  loadingCtr: 0,
  user: null
};

export const rootReducer: ActionReducerMap<RootState> = {
  app: appReducer
};

export const metaReducers: MetaReducer<RootState>[] = !environment.production
  ? [logger, storeFreeze]
  : [];

export function appReducer(
  state = initialAppState,
  action: AppAction
): AppState {
  switch (action.type) {
    case AppActionType.Login:
      return state;
    case AppActionType.LoginSuccess:
      return { ...state, user: action.payload.user };
    case AppActionType.LoginError:
      return { ...state, error: action.payload.error };
    case AppActionType.Logout:
      return { ...state, user: null };
    case AppActionType.LoadingShow:
      return { ...state, loading: true, loadingCtr: state.loadingCtr + 1 };
    case AppActionType.LoadingHide:
      return { ...state, loading: false, loadingCtr: state.loadingCtr - 1 };
    default:
      return state;
  }
}

// console.log all actions
export function logger(
  reducer: ActionReducer<RootState>
): ActionReducer<RootState> {
  return (state: RootState, action: any): any => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return result;
  };
}
