import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from '../shared/services/auth/auth.service';
import {
  AppAction,
  AppActionType,
  LoginSuccess,
  LoginError,
  LoadingShow,
  LoadingHide
} from './app.action';
import { map, catchError, tap, concatMap } from 'rxjs/operators';
import { of, concat } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from '../shared/component/loading/loading.service';

@Injectable()
export class AppEffect {
  constructor(
    private actions$: Actions<AppAction>,
    private authService: AuthService,
    private router: Router,
    private alert: ToastrService,
    private translate: TranslateService,
    private loadingService: LoadingService
  ) {}

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AppActionType.Login),
    map(action => action.payload),
    concatMap(payload =>
      concat(
        of(new LoadingShow()),
        this.authService.login(payload.username, payload.password).pipe(
          map(user => new LoginSuccess({ user })),
          catchError(error => of(new LoginError({ error })))
        )
      )
    )
  );

  @Effect()
  loginSuccess$ = this.actions$.pipe(
    ofType(AppActionType.LoginSuccess),
    tap(() => this.router.navigate(['/'])),
    map(() => new LoadingHide())
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AppActionType.Logout),
    tap(() => localStorage.removeItem('access-token')),
    tap(() => this.router.navigate(['/login']))
  );

  @Effect({ dispatch: false })
  loadingShow$ = this.actions$.pipe(
    ofType(AppActionType.LoadingShow),
    tap(() => this.loadingService.show())
  );

  @Effect({ dispatch: false })
  loadingHide$ = this.actions$.pipe(
    ofType(AppActionType.LoadingHide),
    tap(() => this.loadingService.hide())
  );

  @Effect()
  error$ = this.actions$.pipe(
    ofType(AppActionType.LoginError),
    map(action => action.payload.error),
    tap(error => {
      this.alert.error(
        error.responseMessage,
        this.translate.instant('lb.errormessage')
      );
    }),
    map(() => new LoadingHide())
  );
}
