import { Action } from '@ngrx/store';
import { User } from '../shared/model/user';

export enum AppActionType {
  Login = '[App] Login',
  LoginSuccess = '[App] Login success',
  LoginError = '[App] Login error',
  Logout = '[App] Logout',
  LoadingShow = '[App] loading show',
  LoadingHide = '[App] loading hide'
}

export class Login implements Action {
  readonly type = AppActionType.Login;

  constructor(public payload: { username: string; password: string }) {}
}

export class LoginSuccess implements Action {
  readonly type = AppActionType.LoginSuccess;

  constructor(public payload: { user: User }) {}
}

export class LoginError implements Action {
  readonly type = AppActionType.LoginError;

  constructor(public payload: { error: any }) {}
}

export class Logout implements Action {
  readonly type = AppActionType.Logout;
}

export class LoadingShow implements Action {
  readonly type = AppActionType.LoadingShow;
}

export class LoadingHide implements Action {
  readonly type = AppActionType.LoadingHide;
}

export type AppAction =
  | Login
  | LoginSuccess
  | LoginError
  | Logout
  | LoadingShow
  | LoadingHide;
