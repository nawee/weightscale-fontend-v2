interface LineChartData {
  date: Date;
  value: number;
}
