import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnChanges, OnInit {
  @Input()
  data: LineChartData[];
  private padding = [30, 40];
  private xScale: any;
  private yScale: any;
  private xAxis: any;
  private yAxis: any;
  private width: any;
  private height: any;

  constructor() {}
  ngOnInit(): void {
    this.createChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.data.previousValue !== undefined &&
      !changes.data.isFirstChange()
    ) {
      this.updateChart();
    }
  }

  private updateChart() {
    console.log('udpate chart');
    this.xScale.domain(d3.extent(this.data, d => d.date));
    this.yScale.domain(d3.extent(this.data, d => d.value));
    this.xAxis.transition().call(d3.axisBottom(this.xScale));
    this.yAxis.transition().call(d3.axisLeft(this.yScale));

    const svg = d3.select('#svg').transition();
    svg
      .select('.lineChart') // change the line
      .duration(750)
      .attr(
        'd',
        d3
          .line<LineChartData>()
          .x(d => this.xScale(d.date))
          .y(d => this.yScale(d.value))(this.data)
      );
  }

  private createChart() {
    const svg = d3.select('#svg');
    const container = d3.select('#container');

    this.width = (svg.node() as HTMLElement).getBoundingClientRect().width;
    this.height = (svg.node() as HTMLElement).getBoundingClientRect().height;

    this.xScale = d3
      .scaleTime()
      .domain(d3.extent(this.data, d => d.date))
      .range([0 + this.padding[1], this.width - this.padding[0]]);
    this.xAxis = svg
      .append('g')
      .attr('transform', 'translate(0,' + (this.height - this.padding[1]) + ')')
      .call(d3.axisBottom(this.xScale));

    this.yScale = d3
      .scaleLinear()
      .domain(d3.extent(this.data, d => d.value))
      .range([this.height - this.padding[1], 0 + this.padding[0]]);
    this.yAxis = svg
      .append('g')
      .attr('transform', 'translate(' + this.padding[1] + ', 0)')
      .call(d3.axisLeft(this.yScale));

    // svg
    //   .append('g')
    //   .attr('class', 'grid')
    //   .attr('transform', 'translate(0,' + (this.height - this.padding[1]) + ')')
    //   .call(
    //     d3
    //       .axisBottom(this.xScale)
    //       .tickSize(-(this.height - this.padding[1] - this.padding[0]))
    //   )
    //   .style('color', 'grey')
    //   .style('opacity', 0.2)
    //   .selectAll('text')
    //   .remove();

    svg
      .append('path')
      .datum(this.data)
      .attr('class', 'lineChart')
      .attr('fill', 'none')
      .attr('stroke', 'steelblue')
      .attr('stroke-width', 1.5)
      .attr(
        'd',
        d3
          .line<LineChartData>()
          .x(d => this.xScale(d.date))
          .y(d => this.yScale(d.value))
      );

    // svg
    //   .selectAll('circle-group')
    //   .data(this.data)
    //   .enter()
    //   .append('circle')
    //   .attr('cx', d => this.xScale(d.date))
    //   .attr('cy', d => this.yScale(d.value))
    //   .attr('r', 5)
    //   .attr('fill', '#8FE182');

    // svg
    //   .selectAll('vertical-lines')
    //   .data(this.data)
    //   .enter()
    //   .append('rect')
    //   .attr('x', d => this.xScale(d.date) - 45 / 2)
    //   .attr('y', this.padding[0])
    //   .attr('width', '45px')
    //   .attr('height', this.height - this.padding[0] - this.padding[1])
    //   .attr('fill', 'transparent')
    //   .style('cursor', 'pointer')
    //   .on('mouseover', d => {
    //     container
    //       .append('div')
    //       .attr('class', 'tooltip')
    //       .style('position', 'absolute')
    //       .style('left', this.xScale(d.date) - 50 + 'px')
    //       .style('top', '-20px')
    //       .style('background-color', 'white')
    //       .style('color', 'black')
    //       .style('padding', '5px 0px')
    //       .style('border-radius', '5px')
    //       .style('width', '100px')
    //       .style('text-align', 'center')
    //       .html(`${d.value}`);
    //   })
    //   .on('mouseout', function() {
    //     container.selectAll('.tooltip').remove();
    //   });
  }
}
