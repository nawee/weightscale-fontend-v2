import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  constructor() {}

  loaderSubject = new Subject<boolean>();
  private count = 0;

  show() {
    this.count++;
    this.loaderSubject.next(true);
  }

  hide() {
    this.count--;
    if (this.count <= 0) {
      this.count = 0;
      this.loaderSubject.next(false);
    }
  }
}
