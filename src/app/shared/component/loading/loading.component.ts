import { LoadingService } from './loading.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {
  show = false;

  constructor(private loadingService: LoadingService) {}

  ngOnInit() {
    this.loadingService.loaderSubject.subscribe(flag => {
      this.show = flag;
    });
  }
}
