import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusBoxComponent } from './statusBox.component';
import { MatCardModule } from '@angular/material';
import { MatGridListModule, MatIconModule } from '@angular/material';

@NgModule({
  imports: [CommonModule, MatCardModule, MatGridListModule, MatIconModule],
  declarations: [StatusBoxComponent],
  exports: [StatusBoxComponent]
})
export class StatusBoxModule {}
