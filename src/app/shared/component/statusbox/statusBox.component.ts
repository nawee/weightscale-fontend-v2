import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-statusbox',
  templateUrl: './statusBox.component.html',
  styleUrls: ['./statusBox.component.scss']
})
export class StatusBoxComponent implements OnInit {
  @Input() bgClass: string;
  @Input() icon: string;
  @Input() count: number;
  @Input() label: string;
  @Input() data: number;

  constructor() {}

  ngOnInit() {}
}
