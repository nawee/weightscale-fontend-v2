import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private apiUrl = environment.apiUrl;

  constructor() { }

  getApiUrl(): String {
      return this.apiUrl;
  }
}
