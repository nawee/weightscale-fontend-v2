import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { Observable, throwError } from 'rxjs';
import { map, tap, catchError, delay } from 'rxjs/operators';
import { User } from '../../model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private config: ConfigService) {}

  login(username: string, password: string): Observable<User> {
    return this.http
      .post(
        this.config.getApiUrl() + '/login',
        { username, password },
        { observe: 'response' }
      )
      .pipe(
        tap((resp: HttpResponse<any>) => {
          this.setTokenFromResponse(resp);
        }),
        map((resp: HttpResponse<any>) => resp.body),
        catchError(this.handleError.bind(this))
      );
  }

  private storeLoginStatus(flag: boolean) {
    localStorage.setItem('isLogin', '' + flag);
  }

  private removeLoginStatus() {
    localStorage.removeItem('isLogin');
  }

  public getLoginStatus(): boolean {
    return localStorage.getItem('isLogin') === 'true' ? true : false;
  }

  private storeToken(token: string) {
    localStorage.setItem('access-token', token);
  }

  private removeToken() {
    localStorage.removeItem('access-token');
  }

  private setTokenFromResponse(resp: HttpResponse<any>) {
    const tokenHeader = resp.headers.get('Authorization');
    const token = tokenHeader.match(/Bearer (.*)/)[1];
    this.storeToken(token);
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(error.message);
    }
    this.removeToken();
    return throwError(error.error.responseStatus);
  }
}
