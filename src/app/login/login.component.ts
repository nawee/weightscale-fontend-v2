import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.reducer';
import { Login } from '../store/app.action';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AbstractForm } from '../shared/model/abstractForm';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends AbstractForm implements OnInit {
  form: FormGroup;
  formErrors = {
    username: '',
    password: ''
  };
  validationMessages = {
    username: {
      required: this.translate.instant('lb.requiredusername')
    },
    password: {
      required: this.translate.instant('lb.requiredpassword')
    }
  };
  constructor(
    private store: Store<AppState>,
    private formbuilder: FormBuilder,
    private translate: TranslateService
  ) {
    super();
  }

  ngOnInit() {
    this.formInit();
  }

  login() {
    const { username, password } = this.form.value;
    this.store.dispatch(new Login({ username, password }));
  }

  private formInit() {
    this.form = this.formbuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.subscribeToFormChanged();
    this.onValueChanged();
  }
}
